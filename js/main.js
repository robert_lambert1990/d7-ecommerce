(function ($) {




  $(document).mouseup(function (e)
  {
      var container = $(".shoppingcart");

      if (!container.is(e.target) // if the target of the click isn't the container...
          && container.has(e.target).length === 0) // ... nor a descendant of the container
      {
          container.hide();
      }
  });
  $(window).on('resize', function(){

    var height = $('.home-topleft').height();
    $('.home-topright').height(height);
  });
 

  $(document).ready(function(){

    
       $('.checkoutcontent form #edit-login-pane-name').on('input', function(){
          var div = $(this);
          changeBtn(div, '.checkoutcontent form #edit-login-pane-pass', '.checkoutcontent form .checkout-continue');
      });

      $('.checkoutcontent form #edit-login-pane-pass').on('input', function(){
            var div = $(this);
          changeBtn(div, '.checkoutcontent form #edit-login-pane-name', '.checkoutcontent form .checkout-continue');
      });



    //For now

 //$('.addtobasket form button').remove();

    //to here

var countWindow = 0;
window.setInterval(function(){
if (countWindow < 5) {
var height = $('.home-topleft').height();
$('.home-topright').height(height);
         countWindow++;
    };
}, 10);

     $("#myCarousel").swiperight(function() {  
              $(this).carousel('prev');  
                });  
           $("#myCarousel").swipeleft(function() {  
              $(this).carousel('next');  
       }); 



    if($('.selection .pane-title').css('background-size') == 'cover'){
      $('.selection .pane-content').attr('id', 'hidden');
    } else {
      $('.selection .pane-title').attr('id', 'active');
      $('.selection .pane-content').attr('id', '')
    }
    $('.selection .pane-title').on('click', function(){
      if($('.selection .pane-content').attr('id') == '') {
        $('.selection .pane-content').attr('id', 'hidden');
        $('.selection .pane-title').attr('id', '');
      }
      else {
        $('.selection .pane-content').attr('id', '');
        $('.selection .pane-title').attr('id', 'active');
      }


 
   

      
  });

    $('.checkoutcontent form#commerce-checkout-form-login span.panel-title').html('If you have an account enter details below - Or checkout as guest');
      $('.shoppingcart .cross').on('click', function(){
          $('.shoppingcart').toggle();
      });

    $('.pane-search-api-page-search-page .form-submit').html('<i class="fa fa-search"></i>');
    $('.main-content .content img').attr('class', 'img-responsive');

    $('.destitle').on('click', function(){
        displayContent('.descontent');
    });

    $('.kftitle').on('click', function(){
        displayContent('.kfcontent');
    })

    $('.fittingtitle').on('click', function(){
        displayContent('.fittingcontent');    
    })


    $('.reviewstitle').on('click', function(){
        displayContent('.reviewscontent');      
    })


    $('.pctitle').on('click', function(){
        displayContent('.pccontent');     
    })

    $('.shoppingbasket').on('click', function(){
        $('.shoppingcart').toggle();
    });

    $('.imageswap').on('click', function(){
        $img = $(this).attr('src');    
        $('.selectedimage img').attr('src', $img);
    });

    $('#block-system-main').on('change', function(){
      console.log("change");
    });

    $('button[name="guest"]').remove();

});
function reset() {
    $('.descriptions').each(function(){
        $(this).css('display', 'none');

    });
}

function displayContent(div) {
   if($(div).css('display') == 'block') {
      reset();
  }
  else {
      reset();
      $(div).toggle();
  }
}


function changeBtn(div1, div2, btn) {
   if(!$(div1).val() && !$(div2).val()) {
            $(btn).html('Checkout as a guest');
        }
        else {
          $(btn).html('Log in');
        }
}

})(jQuery);
