<?php

/**
 * @file
 * Default theme implementation for profiles.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) profile type label.
 * - $url: The URL to view the current profile.
 * - $page: TRUE if this is the main view page $url points too.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-profile
 *   - profile-{TYPE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="profileruser <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
<?php
  global $user; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <p><span class="profiletitle">Title:</span> <?php print $content['field_title'][0]['#markup']; ?></p>
    <p><span class="profiletitle">First Name:</span> <?php print $content['field_first_name'][0]['#markup']; ?></p>
    <p><span class="profiletitle">Last Name:</span> <?php print $content['field_last_name'][0]['#markup']; ?></p>
    <p><span class="profiletitle">Email:</span> <?php print $user->mail; ?></p>
    <p><span class="profiletitle">Moible Number:</span> <?php print $content['field_mobile_number'][0]['#markup']; ?></p>
  </div>
</div>
