<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
$productid = $content['field_shoe_reference'][0]['product_id']['#value'];
$productload =  commerce_product_load($productid);
$block = block_get_blocks_by_region('highlighted');
$block2 = block_get_blocks_by_region('shoppingbasket');

?>
<?php print $messages; ?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="backbtn">
                            <a href="/shop-now" class="continue">Back to collection</a> 
                        </div>
                        
                    </div>
                </div>
                <div class="row hidden-xs">
                    <div class="col-sm-3">                   
                       
                        <?php $images = $content['field_image']['#items']; ?>
                        <?php foreach ($images as $image => $value): ?>
                           <img class="img-responsive imageswap" src="<?php print file_create_url($value['uri']); ?>" alt="">
                        <?php endforeach; ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="selectedimage">
                            <img class="img-responsive" src="<?php print file_create_url($images['0']['uri']); ?>" alt="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 visible-xs">
                        <div id="myCarousel" class="carousel slide shoeimgxs" data-ride="carousel">
                          <ol class="carousel-indicators">
                            <?php foreach ($images as $image => $value): ?>
                                <li data-target="#myCarousel" data-slide-to="<?php echo $image; ?>" class="<?php if($image == 0): ?>active<?php endif; ?>"></li>  
                            <?php endforeach; ?>                         
                          </ol>
                              <!-- Wrapper for slides -->
                              <div class="carousel-inner" role="listbox">
                                <?php foreach ($images as $image => $value): ?>
                                    <div class="item <?php if($image == 0): ?>active<?php endif; ?>">
                                        <img class="img-responsive imageswap" src="<?php print file_create_url($value['uri']); ?>" alt="">
                                    </div>
                                <?php endforeach; ?>
                               
                                                                 
                              </div>
                            </div>
                    </div>
                </div>                
            </div>
            <div class="col-sm-5 col-sm-offset-1 addtobasket">
                <?php $value = $productload->commerce_price['und'][0]['amount']; ?>
               
                <h2 class="title"<?php print $title_attributes; ?>><?php print $title; ?> </h2>      
                <div class="shoeprice">£<?php print number_format($value/100, 2, '.', ''); ?></div>

                
          
              

                <?php print render($content['field_shoe_reference']); ?>
                                      
                <div class="share">
                     <span class='st_facebook' displayText='Facebook'></span>
                    <span class='st_twitter' displayText='Tweet'></span>
                    <span class='st_pinterest' displayText='Pinterest'></span>
                    <span class='st_googleplus' displayText='Google +'></span>
                </div>

                    
                <div class="panel-group" id="accordion">  
                    <div class="panel panel-default">
                        <div class="panel-heading first">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                                <h4 class="panel-title">
                                Description     &nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true">
                            <div class="panel-body">
                                <?php print render($content['field_content']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading second">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                <h4 class="panel-title">
                                Details     &nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <?php print render ($content['field_key_features']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading third">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                <h4 class="panel-title">
                                Size & Fit  &nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <?php print render ($content['field_fitting']); ?>
                            </div>
                        </div>
                    </div>                   
                    <div class="panel panel-default">
                        <div class="panel-heading fourth">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseDive">
                                <h4 class="panel-title">
                                Product Care    &nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseDive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <?php print render ($content['field_product_care']); ?>
                            </div>
                        </div>
                    </div>               
                </div>
            </div>
        </div> 
    </div>
</div>
<style>
.node-shoe img {width: 100%}
</style>

<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "401018e4-ea8d-456b-a0fe-2f47450df2d4", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>