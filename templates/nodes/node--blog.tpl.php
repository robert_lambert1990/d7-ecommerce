<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
$block = block_get_blocks_by_region('sidebar_second');


?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
<?php $footer_left = block_get_blocks_by_region('footer_left'); 
	  $footer_center = block_get_blocks_by_region('footer_middle');
	  $footer_right = block_get_blocks_by_region('footer_right');
	  $blog_top = block_get_blocks_by_region('blog_top');
	  $blog_side = block_get_blocks_by_region('blog_side');    
      $blogbanner = block_get_blocks_by_region('blog_banner');    
      $blog_social = block_get_blocks_by_region('blog_social');    
      $blog_disqus = block_get_blocks_by_region('blog_disqus'); 
      $blog_search = block_get_blocks_by_region('blog_search'); 
?>
<div class="blogheader">
<?php print render ($blogbanner); ?>
</div>

<div class="container">

<div class="row">
    <div class="col-sm-12 blogtop">
        <div class="<?php if(($blog_top['bean_blog-categories-top']['bean']['blog-categories-top']['field_links'][0]['#element']['title']) == ($content['field_blog_type'][0]['#title'])): ?> active <?php endif; ?>">
            <?php print render ($blog_top['bean_blog-categories-top']['bean']['blog-categories-top']['field_links'][0]); ?> |
        </div>  
        <div class="<?php if(($blog_top['bean_blog-categories-top']['bean']['blog-categories-top']['field_links'][1]['#element']['title']) == ($content['field_blog_type'][0]['#title'])): ?> active <?php endif; ?>">
            <?php print render ($blog_top['bean_blog-categories-top']['bean']['blog-categories-top']['field_links'][1]); ?> |
        </div>
            <div class="<?php if(($blog_top['bean_blog-categories-top']['bean']['blog-categories-top']['field_links'][2]['#element']['title']) == ($content['field_blog_type'][0]['#title'])): ?> active <?php endif; ?>">
            <?php print render ($blog_top['bean_blog-categories-top']['bean']['blog-categories-top']['field_links'][2]); ?>
        </div>   
    </div>
</div>
<div class="row">
    <div class="col-md-8 blog-content">       
        <h1><?php print $title; ?></h1>
        <?php print render ($content['field_month_and_year']); ?>
    </div>
</div>
    <div class="row">
        <div class="col-md-9 col-sm-12 blog-content">       	

        
            <?php if (isset($content['field_date'])): ?>   
        	   <h3><?php print render ($content['field_date']); ?></h3><br>
            <?php endif ?>
            <div class="featuredblogimg">
                 <?php print render ($content['field_featured_image']); ?>
            </div>
              
            <?php print render ($content['body']); ?>
              <?php print render($content['links']); ?>

  <?php print render($blog_disqus); ?>
        </div>   
        <div class="col-md-3 hidden-sm hidden-xs blog-side blogartside">  
                    <div class="blog-page-recent pane-recent-blogs">                              
                        <?php print render ($blog_side); ?>           
                    </div>
                    
                            <?php print render ($blog_search); ?>
                        
                    <div class="followusblog">
                         <?php print render ($blog_social); ?>   
                    </div>
        </div>
    </div>
</div>
</div>
