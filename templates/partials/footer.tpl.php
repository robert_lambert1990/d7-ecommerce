

<footer class="footer2">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="joinourfamily">                
                     <?php print render($page['secondary_fot']); ?>
                    </div>
                </div>
            </div>
        </div>
    <div class="bottomfooter">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 visible-xs">
                    <?php print render($page['footer_rright']); ?>
                </div>
                <div class="col-sm-7">
                    <div class="row">                  
                        <div class="col-sm-4">
                            <?php print render($page['footer_left']); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php print render($page['footer_middle']); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php print render($page['footer_right']); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-sm-offset-1 hidden-xs">
                    <?php print render($page['footer_rright']); ?>
                </div>
            </div>
        </div>
    </div>
      <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <p>This website and its content is copyright of Calla - © Calla <?php echo date("Y"); ?> All rights reserved.</p>
      </div>
     </div>
  </div>
</footer>