<header>
  <div class="container logo-area">
    <div class="row">
      <div class="col-sm-2 col-xs-7">
                 <div class="logo">
                        <a href="<?php print $front_page;?>"><img alt="Calla bunion shoes" src="<?php print $logo;?>"></a>

                     </div>             
      </div>
      <div class="col-sm-10 col-xs-5 topright">
        <div class="row">
          <div class="col-xs-12">
              <?php global $user; ?>
                <div class="signin">
                  <a href="/user"><?php if($user->uid): ?>Your account<?php else: ?>Sign in<?php endif; ?></a>
                </div>            
                <?php if($user->uid): ?>
                  <div class="signin"><span class="spacer">|</span><a href="/user/logout">Logout</a></div>
                <?php endif; ?>
          </div>
        </div>
        <div class="row">
          <div class="visible-xs col-xs-12">
            <nav class="navbar-default navbar_button" role="navigation">
    
                  <div class="navbar-header">                
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                      <div class="menu-text">Menu</div>
                      <div class="burger-menu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </div>
                    </button>
                </div>
              </nav>
        </div>
        </div>
        
      </div>
      
    </div>
  </div>
  <nav class="main_nav navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              
        <div class="mainnav">
          <?php print render($primary_nav); ?>   
        </div>
      </div>
  </nav>
</header>
