<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup templates
 */
?>
<header>
  <div class="container logo-area">
    <div class="row">
      <div class="col-sm-2 col-xs-7">
                 <div class="logo">
                        <a href="<?php print $front_page;?>"><img alt="Calla bunion shoes" src="<?php print $logo;?>"></a>

                     </div>             
      </div>
      <div class="col-sm-10 col-xs-5 topright">
        <div class="row">
          <div class="col-xs-12">
              <?php global $user; ?>
                <!--<div class="signin">
                  <a href="/user"><?php // if($user->uid): ?>Your account<?php // else: ?>Sign in<?php // endif; ?></a> <span class="spacer">|</span>
                </div>
                <div class="shoppingbasketsection">
                  <?php // if($page['cart']): ?>
                    <?php // print render ($page['cart']); ?>
                  <?php // else: ?>
                    <i class="fa fa-shopping-bag" aria-hidden="true"></i> 0
                  <?php // endif; ?>
                </div>
                <?php // if($user->uid): ?>
                  <div class="signin"><span class="spacer">|</span><a href="/user/logout">Logout</a></div>
                <?php // endif; ?>-->
          </div>
        </div>
        <div class="row">
          <div class="visible-xs col-xs-12">
            <nav class="navbar-default navbar_button" role="navigation">
    
                  <div class="navbar-header">                
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                      <div class="menu-text">Menu</div>
                      <div class="burger-menu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </div>
                    </button>
                </div>
              </nav>
        </div>
        </div>
        
      </div>
      
    </div>
  </div>
  <nav class="main_nav navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              
        <div class="mainnav">
          <?php print render($primary_nav); ?>   
        </div>
      </div>
  </nav>
</header>
 
<main>
	<div class="container ">
		<div class="row">
			<div class="col-sm-12">
				<div class="banner">
					<?php print render ($page['banner']); ?>
				</div>
				
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="signinarea">				
									<?php if (!empty($tabs)): ?>
  <?php print render($tabs); ?>
<?php endif; ?>
				
  				<?php print render($page['content']); ?>
  				</div>
  			</div>
  		</div>
  	</div>         
</main>

<?php

	global $user;
	if ( $user->uid ) {
	$uid = $user->uid;
	$account = user_load($uid);
	$profile = profile2_load_by_user($account, 'main');
	if($profile) {
		foreach($commerce_orders as $commerce_order) {	 
		  $commerceprofile = commerce_customer_profile_load($commerce_order->commerce_customer_billing['und'][0][profile_id]);
		}



			$title = $commerceprofile->field_title['und'][0]['value'];
			$firstname = $commerceprofile->commerce_customer_address['und'][0]['first_name'];
			$lastname = $commerceprofile->commerce_customer_address['und'][0]['last_name'];
			$mobile = $commerceprofile->field_mobile_number['und'][0]['value'];

			$firstname ? $profile->field_first_name['und'][0]['value'] = $firstname : '';
			$firstname ? $profile->field_first_name['und'][0]['safe_value'] = $firstname : '';
			$lastname ? $profile->field_last_name['und'][0]['value']= $lastname : '';
			$lastname ? $profile->field_last_name['und'][0]['safe_value']= $lastname : '';
			$mobile ? $profile->field_mobile_number['und'][0]['value'] = $mobile : '';
			$mobile ? $profile->field_mobile_number['und'][0]['safe_value'] = $mobile : '';
			$title ? $profile->field_title['und'][0]['value'] = $title : '';

			profile2_save($profile);
		};	
	};
	
?>

<?php include_once DRUPAL_ROOT . '/sites/all/themes/store/templates/partials/footer.tpl.php'; ?>