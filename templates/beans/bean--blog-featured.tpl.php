<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

?>
<div class="hidden-xs <?php print $classes; ?> clearfix"<?php print $attributes; ?> >

  <div class="content"<?php print $content_attributes; ?>>
  <?php $node1 = ($content['field_featured_blogs']['#object']->field_featured_blogs['und'][0]['entity']->nid); ?>
  <?php $nodeload1 = node_load($node1); ?>
  <?php $node2 = ($content['field_featured_blogs']['#object']->field_featured_blogs['und'][1]['entity']->nid); ?>
  <?php $nodeload2 = node_load($node2); ?>
  <?php $node3 = ($content['field_featured_blogs']['#object']->field_featured_blogs['und'][2]['entity']->nid); ?>
  <?php $nodeload3 = node_load($node3); ?>
  <?php $node4 = ($content['field_featured_blogs']['#object']->field_featured_blogs['und'][3]['entity']->nid); ?>
  <?php $nodeload4 = node_load($node4);

   ?>
  <?php 
    $img_url1 = $nodeload1->field_featured_image['und'][0]['uri'];
    $style1 = 'featured_blog_image';
    $img_url2 = $nodeload2->field_featured_image['und'][0]['uri'];
    $style2 = 'featured_blog_image';
    $img_url3 = $nodeload3->field_featured_image['und'][0]['uri'];
    $style3 = 'featured_blog_image';
    $img_url4 = $nodeload4->field_featured_image['und'][0]['uri'];
    $style4 = 'featured_blog_image';

    $taxonomy1 = taxonomy_term_load($nodeload1->field_blog_type['und'][0]['tid']); 
    $taxonomy2 = taxonomy_term_load($nodeload2->field_blog_type['und'][0]['tid']); 
    $taxonomy3 = taxonomy_term_load($nodeload3->field_blog_type['und'][0]['tid']); 
    $taxonomy4 = taxonomy_term_load($nodeload4->field_blog_type['und'][0]['tid']); 

    $node1url = drupal_get_path_alias('node/' . $node1);
    $node2url = drupal_get_path_alias('node/' . $node2);
    $node3url = drupal_get_path_alias('node/' . $node3);
    $node4url = drupal_get_path_alias('node/' . $node4);


   ?>
  
  
  <div class="row">
  	<div class="col-md-7 col-sm-7 featuredtopimage">
  		<div class="loadedblog">
  			 <div id="img1">
            <img src="<?php print file_create_url($img_url1); ?>">
         </div>
          <div id="img2" class="hideimg">
          <img src="<?php print file_create_url($img_url2); ?>">
         </div>
          <div id="img3" class="hideimg">
            <img src="<?php print file_create_url($img_url3); ?>">
          </div>
          <div id="img4" class="hideimg">
             <img src="<?php print file_create_url($img_url4); ?>">
          </div>
  		</div>
  	</div>
   	<div class="col-md-5 col-sm-5 featuredblogbuttons">
   		<div class="row"> 		
   			<div rel="nodeload" class="col-sm-12 nodeload1" id="active">
   				<a href="<?php print $node1url; ?>">
            <span class="taxname">
              <?php print $taxonomy1->name; ?></span><br>
   					<?php print $nodeload1->title; ?>
   				</a>
   			</div>
   		</div>
    	<div rel="nodeload" class="row"> 		
   			<div class="col-sm-12 nodeload2">
   				<a href="<?php print $node2url; ?>">
            <span class="taxname">
              <?php print $taxonomy2->name; ?></span><br>
   					<?php print $nodeload2->title; ?>
   				</a>
   			</div>
   		</div>
   		<div class="row"> 		
   			<div rel="nodeload" class="col-sm-12 nodeload3">        
   				<a href="<?php print $node3url; ?>">
            <span class="taxname">
              <?php print $taxonomy3->name; ?></span><br>
   					<?php print $nodeload3->title; ?>
   				</a>
   			</div>
   		</div>  		
      <div class="row hidden-sm">     
        <div rel="nodeload" class="col-sm-12 nodeload4">
          <a href="<?php print $node4url; ?>">
            <span class="taxname">
              <?php print $taxonomy4->name; ?></span><br>
            <?php print $nodeload4->title; ?>
          </a>
        </div>
      </div>      
  	</div> 	
  </div>
  </div>
</div>

<script>
	$ = jQuery;
	// $('div[rel]').on('mouseover', function(){
 //    test = $(this);
 //    $number = ($(test).attr('class'));
 //    console.log($number);
 //    featuredImage($number);

 //  });
 //  function featuredImage(number) {
 //    $('.nodeload1').attr('id', '');
 //    $('.nodeload2').attr('id', '');
 //    $('.nodeload3').attr('id', '');
 //    $('.nodeload4').attr('id', '');
 //    $('#img4').attr('class', 'hideimg');
 //    $('#img4').attr('class', 'hideimg');
 //    $('#img4').attr('class', 'hideimg');
 //    $('#img4').attr('class', 'hideimg');
 //    $(number).attr('id', 'active');
   

 //  }



 //  $(document).ready(function(){
 //    maxHeight('featuredblogbuttons', 'featuredtopimage');
 //  });
 //  function maxHeight(tallest, shorter) {
 //    var max = $('.' + tallest).height();
 //    $('.' + shorter).height(max);
 //  }
 //  

   $('.nodeload1').on('mouseover', function(){
    $(this).attr('id', 'active');
    $('.nodeload2').attr('id', '');
    $('.nodeload3').attr('id', '');
    $('.nodeload4').attr('id', '');

        $('#img1').attr('class', 'showimg');
      $('#img2').attr('class', 'hideimg');
      $('#img3').attr('class', 'hideimg');
      $('#img4').attr('class', 'hideimg');
        });
  $('.nodeload2').on('mouseover', function(){
    $(this).attr('id', 'active');
    $('.nodeload1').attr('id', '');
    $('.nodeload3').attr('id', '');
    $('.nodeload4').attr('id', '');
      $('#img2').attr('class', 'showimg');
      $('#img1').attr('class', 'hideimg');
      $('#img3').attr('class', 'hideimg');
      $('#img4').attr('class', 'hideimg');
  });
  $('.nodeload3').on('mouseover', function(){
    $(this).attr('id', 'active');
    $('.nodeload2').attr('id', '');
    $('.nodeload1').attr('id', '');
    $('.nodeload4').attr('id', '');
      $('#img3').attr('class', 'showimg');
      $('#img2').attr('class', 'hideimg');
      $('#img1').attr('class', 'hideimg');
      $('#img4').attr('class', 'hideimg');
  });
  $('.nodeload4').on('mouseover', function(){
    $(this).attr('id', 'active');
    $('.nodeload2').attr('id', '');
    $('.nodeload3').attr('id', '');
    $('.nodeload1').attr('id', '');
      $('#img4').attr('class', 'showimg');
      $('#img2').attr('class', 'hideimg');
      $('#img1').attr('class', 'hideimg');
      $('#img3').attr('class', 'hideimg');
  });
  $(document).ready(function(){
    maxHeight('featuredblogbuttons', 'featuredtopimage');
  });
  function maxHeight(tallest, shorter) {
    var max = $('.' + tallest).height();
    $('.' + shorter).height(max);
  }
</script>
