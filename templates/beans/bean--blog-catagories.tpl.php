<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="content"<?php print $content_attributes; ?>>

	<?php 
		$field_values = array('1' , '2', '3'); 
		$i = 0;

	?>
	<div class="row">
		<?php foreach ($field_values as $field_value): ?>
		<div class="col-md-4">
		  	<div class="row">
		  		<div class="col-md-12 col-sm-4 home-blog-image">
		  			<a href="<?php print render ($content['field_link_blog_cat'][$i]['#element']['url']); ?>">
		  				<?php print render ($content['field_image_blog_cat'][$i]); ?>
		  			</a>
				</div>				
				<div class="col-md-12 col-sm-8 catinfo">
					<div class="cattitle">
			  			<a href="<?php print render ($content['field_link_blog_cat'][$i]['#element']['url']); ?>">
			  				<?php print render ($content['field_term_reference'][$i]['#title']); ?>
			  			</a><br>
			  		</div>
		  			<?php print render ($content['field_body'][$i]['#markup']); ?>
		  		</div>
		  	</div>
		 </div>
		  	<?php $i++; ?>
		<?php endforeach; ?>
	</div>
  </div>
</div>
