<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
$price = $fields['commerce_unit_price']->content;
$quantity = $fields['quantity']->raw;
$totalprice = ($price * $quantity)/100;
?>
<div class="row shoppingbag-item">
	<div class="col-sm-5">
		<?php print $fields['field_product_reference']->content; ?>
	</div>
	<div class="col-sm-7">
		<h3><?php print $fields['title']->content; ?></h3>
		<div class="heeltype">
			<?php print $fields['field_shoe_heel']->content; ?>
			<div><?php print $fields['field_shoe_type']->content; ?></div>
		</div>
		<div class="shoedetails">
			<p>Color: <?php print $fields['field_shoe_colour']->content; ?></p>
			<p><?php print $fields['field_shoe_size']->content; ?></p>
			<p>Quantity: <?php print $fields['quantity']->content; ?></p>
		</div>
		<div class="price">
			<p>Price:  &pound;<?php print number_format(($totalprice), 2); ?></p>
		</div>
		<div class="remove">
			<?php print $fields['edit_delete']->content; ?>
		</div>			
	</div>
</div>
<div class="row">
	<div class="col-sm-10 col-sm-offset-1 borderbreak"></div>
</div>
<?php

