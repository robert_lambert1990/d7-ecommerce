<?php

// Plugin definition
$plugin = array(
  'title' => t('Blog'),
  'category' => t('Calla Shoes'),
  'icon' => 'twocol.png',
  'theme' => 'panels_blog',
  'css' => 'blog.css',
  'regions' => array( 	
    'banner' => t('Banner'),
    'top' => t('Top'),
    'bottomleft' => t('Bottom Left'),
    'bottomright' => t('Bottom Right')
  ),
);
