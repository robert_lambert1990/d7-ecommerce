<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * each column roughly equal in width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 */
?>
<div class="blogheader">
<?php print $content['banner']; ?>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<?php print $content['top']; ?>
		</div>		
	</div>
	<div class="row bottom-row-blog">
		<div class="col-md-8 col-sm-7">
			<?php print $content['bottomleft']; ?>
		</div>		
		<div class="col-md-4 col-sm-5">		
			<?php print $content['bottomright']; ?>
		</div>
	</div>
</div>
