<?php

// Plugin definition
$plugin = array(
  'title' => t('Blog Cat'),
  'category' => t('Calla Shoes'),
  'icon' => 'twocol.png',
  'theme' => 'panels_blogcat',
  'css' => 'blogcat.css',
  'regions' => array( 
    'banner' => t('Banner'),  
    'topbanner' => t('Top Banner'),   	
    'catnav' => t('Cat Navigation'),
  	'side' => t('Side'),
    'main' => t('Main')
  ),
);
