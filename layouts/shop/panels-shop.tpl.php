<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * each column roughly equal in width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 */
?>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<?php print $content['topbanner']; ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-sm-4">
			<div class="row">
				<div class="col-md-9">
					<?php print $content['side'];  ?>			
</div>
			</div>		
	
		</div>
		<div class="col-xs-12 visible-xs">
			<?php print $content['mobilerefine']; ?>
		</div>
		<div class="col-md-9 col-sm-8">			
			<?php print $content['main']; ?>
		</div>		
	</div>
</div>
