<?php

// Plugin definition
$plugin = array(
  'title' => t('Shop'),
  'category' => t('Calla Shoes'),
  'icon' => 'twocol.png',
  'theme' => 'panels_shop',
  'css' => 'shop.css',
  'regions' => array( 
  	'topbanner' => t('Top Banner'),	
  	'side' => t('Side'),
	'trustspot' => t('Trustspot'),
    'main' => t('Main'),
    'mobilerefine' => t('Mobile Refine')
  ),
);
