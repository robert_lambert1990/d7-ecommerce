<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * each column roughly equal in width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 */
?>
<div class="container home-top">
	<div class="row">
		<div class="col-md-8 home-topleft">
			<?php print $content['topleft']; ?>
		</div>
		<div class="col-md-4 home-topright">
			<?php print $content['topright']; ?>
		</div>
	</div>
</div>


<div class="container home-middle">
	<div class="row">
		<div class="col-xs-4">
			<?php print $content['middleleft']; ?>
		</div>
		<div class="col-xs-4">
			<?php print $content['middlecenter']; ?>
		</div>
		<div class="col-xs-4">
			<?php print $content['middleright']; ?>
		</div>
	</div>
</div>

<div class="container home-bottom">
	<div class="row">
		<div class="col-sm-4">
			<?php print $content['bottomleft']; ?>
		</div>
		<div class="col-sm-4">
			<?php print $content['bottomcenter']; ?>
		</div>
		<div class="col-sm-4">
			<?php print $content['bottomright']; ?>
		</div>
	</div>
</div>