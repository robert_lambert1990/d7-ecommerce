<?php

// Plugin definition
$plugin = array(
  'title' => t('Home'),
  'category' => t('Calla Shoes'),
  'icon' => 'twocol.png',
  'theme' => 'panels_home',
  'css' => 'home.css',
  'regions' => array(
 	'topleft' => t('Top left'),
  'topright' => t('Top Right'),
  'middleleft' => t('Middle left'),
  'middlecenter' => t('Middle center'),
  'middleright' => t('Middle right'),
  'bottomleft' => t('Bottom left'),
  'bottomcenter' => t('Bottom center'),
  'bottomright' => t('Bottom right'),
  ),
);
