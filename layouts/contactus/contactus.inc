<?php

$plugin = array(
  'title' => t('Contact Us'),
  'category' => t('Calla Shoes'),
  'icon' => 'twocol.png',
  'theme' => 'panels_contactus',
  'css' => 'contactus.css',
  'regions' => array(
    'topleft' => t('Top left'),
    'topright' => t('Top Right')    
  ),
);
