<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * each column roughly equal in width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 */

?>
<div class="container">
	<div class="row">
		<div class="col-md-3 left-newsletter quicklinks hidden-sm hidden-xs">
			<?php print $content['topleft']; ?>
		</div>
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-12">
					<?php print $content['topright']; ?>
				</div>
			</div>	
		</div>
	</div>
</div>