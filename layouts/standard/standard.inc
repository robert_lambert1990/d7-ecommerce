<?php

// Plugin definition
$plugin = array(
  'title' => t('Standard'),
  'category' => t('Calla Shoes'),
  'icon' => 'twocol.png',
  'theme' => 'panels_standard',
  'css' => 'standard.css',
  'regions' => array(
 	  'topleft' => t('Top left'),
    'topright' => t('Top Right'),
    'bottomright' => t('Bottom right')
  ),
);
