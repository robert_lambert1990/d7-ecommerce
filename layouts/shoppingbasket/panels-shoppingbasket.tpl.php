<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * each column roughly equal in width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 */
global $base_url;
?>
<div class="container shoppingbasketpage">
	<div class="row">
		<div class="col-sm-12 title">
			<h2>Shopping Bag</h2>
			<?php if (!empty($content['left'])): ?>
			 <a class="continue" href="/shop-now">
		        Continue Shopping
		      </a>
		    <?php endif; ?>
		</div>
	</div>
	
	<div class="row">
		<?php if (!empty($content['left'])): ?>
		<div class="col-sm-6 basketleft">
			<?php print $content['left']; ?>
		</div>
		<div class="col-sm-5 col-sm-offset-1">
			<div class="row">
				<div class="checkouttotal line-item-total">
			    <span class="line-item-total-label">
			
			        <div class="col-md-9 col-sm-8 subtotaltext">
			          <h2 class="subtotal">Sub-total</h2><p class="subtotal">(before delivery)</p>
			          <p>You can choose your delivery options at checkout</p>
			        </div>
			        <div class="col-md-3 col-sm-4 totaltable">
			           <?php print $content['right']; ?>
			        </div>
			            
			    </span>
			  </div>
			</div>
			<div class="row">
				<div class="col-sm-12">
		
			<?php print $content['rightbottom']; ?>
			<div class="cardlogos">	
		<table border="0" cellpadding="10" cellspacing="0" align="center"><tr><td align="center"></td></tr><tr><td align="center">
					<p><img class="img-responsive" alt="Worldpay logo" height="33" src="<?php print $base_url; ?>/sites/default/files/poweredbyworldpay.gif" width="139" />&nbsp;&nbsp;</p>

		<p><img class="img-responsive" alt="American Express logo" height="51" src="<?php print $base_url; ?>/sites/default/files/amex-logo2.gif" width="52" />&nbsp;&nbsp;&nbsp;<img class="img-responsive" alt="Maestro logo" height="40" src="<?php print $base_url; ?>/sites/default/files/maestro.gif" width="63" />&nbsp;&nbsp;<img class="img-responsive" alt="Mastercard logo" height="40" src="<?php print $base_url; ?>/sites/default/files/mastercard.gif" width="62" />&nbsp;&nbsp;<img class="img-responsive" alt="Visa logo" height="40" src="<?php print $base_url; ?>/sites/default/files/visa.gif" width="64" />&nbsp;&nbsp;</p></td></tr></table>



				<!-- PayPal Logo --><table border="0" cellpadding="10" cellspacing="0" align="center"><tr><td align="center"></td></tr><tr><td align="center"><a href="https://www.paypal.com/uk/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/uk/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img class="img-responsive" src="https://www.paypalobjects.com/webstatic/mktg/Logo/AM_SbyPP_mc_vs_ms_ae_UK.png" border="0" alt="PayPal Acceptance Mark"></a></td></tr></table><!-- PayPal Logo -->

						</div>
		<?php else: ?>
				
			<div class="col-sm-12">	
				<p>There are currently no items in your basket</p>
				<a class="continue" href="/shop-now">
		        		Continue Shopping
		      	</a>
			</div>

		<?php endif; ?>

		</div>
	</div>
</div>

			
				</div>
			</div>
