<?php

// Plugin definition
$plugin = array(
  'title' => t('shoppingbasket'),
  'category' => t('Calla Shoes'),
  'icon' => 'twocol.png',
  'theme' => 'panels_shoppingbasket',
  'css' => 'shoppingbasket.css',
  'regions' => array(
    'left' => t('Left'),
    'right' => t('Right'),
    'rightbottom' => t('Right Bottom')
  ),
);
