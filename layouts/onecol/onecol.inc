<?php

// Plugin definition
$plugin = array(
  'title' => t('One Col'),
  'category' => t('Calla Shoes'),
  'icon' => 'twocol.png',
  'theme' => 'panels_onecol',
  'css' => 'onecol.css',
  'regions' => array( 	
    'main' => t('Main')
  ),
);
