<?php
/**
 * @file
 * The primary PHP file for this theme.
 */


/**
 * Implements hook_preprocess_HOOK().
 */
function store_preprocess_node(&$variables) {
	$node = $variables['node'];
	$variables['theme_hook_suggestions'][] = 'node__' . $node->type . '__' . $variables['view_mode'];

	if($node->type == 'shoe') {

		$amount = $node->field_amount['und'][0]['value'] ? $node->field_amount['und'][0]['value'] : '';
		$currency = 'GBP';

		$amount_og = array(
		  '#tag' => 'meta',
		  '#attributes' => array(
		    "property" => "product:price:amount",
		    "content" => $amount,
		  ),
		);

		drupal_add_html_head($amount_og,'opengraph_product_amount');

		$currency_og = array(
		  '#tag' => 'meta',
		  '#attributes' => array(
		    "property" => "og:price:currency",
		    "content" => $currency,
		  ),
		);

		drupal_add_html_head($currency_og,'opengraph_product_currency');

		
	}

}

function store_form_alter(&$form, &$form_state, $form_id) {  
	if($form['#id'] == 'views-form-shopping-cart-left-block-1' || $form['#id'] == 'views-form-shopping-basket-total-block-1' || $form['#id'] == 'views-form-shopping-basket-delete-line-item-block-1') {
		$form['actions']['#access'] = 0;  
			
	}
	if($form['#id'] == 'views-form-shopping-basket-delete-line-item-block-1') {
			$i = 0;
			foreach ($form['edit_delete'] as $key => $value) {				
				$form['edit_delete'][$i]['#value'] = '<i class="fa fa-trash" aria-hidden="true"></i><p>Remove</p>';
				$i++;
			}		
		}	
	if($form['#id'] == 'views-form-commerce-cart-block-default--2') {
		$i = 0;
		foreach ($form['edit_delete'] as $key => $value) {
			$form['edit_delete'][$i]['#value'] = '<i class="fa fa-trash" aria-hidden="true"></i>';
			$i++;
		}	
	}
	if($form['#id'] == 'views-form-commerce-cart-block-default--2') {
		$i = 0;
		foreach ($form['edit_delete'] as $key => $value) {
			$form['edit_delete'][$i]['#value'] = '<i class="fa fa-trash" aria-hidden="true"></i>';
			$i++;
		}	
	}
	if($form['#id'] == 'commerce-checkout-form-login') {
		$form['login_pane']['#title'] = 'I have an account: Login below';
		$form['buttons']['continue']['#value'] = "Check out as a guest";
	}


	if($form['#theme'][1] == 'commerce_cart_add_to_cart_form') {
			$form['submit']['#value'] = 'Add to bag';
			
		
	}
	if($form['#id'] == 'commerce-checkout-form-login') {
		
	}

}



/**
 * Implements hook_theme().
 */
function store_theme($existing, $type, $theme, $path) {
	$theme_path = drupal_get_path('theme', 'store');
	return array(
		'calla_header' => array(
			'variables' => [
			],
			'template' => 'header',
			'path' => $theme_path . '/templates/partials'
		),
	);
}

function store_views_pre_render(&$view) {
	if($view->name == 'shopping_basket_delete_line_item') {
		store_views_pre_build($view);
	}  
}


/**
 * Implements hook_views_pre_build().
 */
function store_views_pre_build(&$view) {
 	$view->override_url = base_path() . $_GET['q'];
}

